package com.sinmobile.gogorgeos;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Commons;

/**
 * Created by ToSuccess on 1/17/2017.
 */

public class RestartActivity extends CommonActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!Commons.g_isAppRunning){

            Intent goMain = new Intent(this, MainActivity.class);
            startActivity(goMain);
        }

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }
    @Override
    public void onClick(View v) {

    }
}
