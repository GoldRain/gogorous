package com.sinmobile.gogorgeos.activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.fragment.FAQ.FAQListFragment;
import com.sinmobile.gogorgeos.fragment.FAQ.FaqContentFragment;
import com.sinmobile.gogorgeos.model.FaqModel;

public class FAQActivity extends CommonActivity  implements View.OnClickListener{

    ImageView ui_imvBack;
    boolean _isContent = false;

    MainActivity _ma;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView) findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        FAQListFragment();
    }

    public void FAQListFragment(){

        FAQListFragment fragment = new FAQListFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }
    public void FaqContentFragment(FaqModel entity){

        _isContent = true;
        FaqContentFragment fragment = new FaqContentFragment(entity);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                gotoMain();
                break;
        }

    }

    private void gotoMain(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

        if (_isContent == true)
        {
            FAQListFragment();
            _isContent = false;

        } else{

            gotoMain();
        }

    }
}
