package com.sinmobile.gogorgeos.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.adapter.LangSpinnerAdapter;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.preference.PrefConst;
import com.sinmobile.gogorgeos.preference.Preference;

public class MyAccountActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    TextView txv_sibmit, txv_name;

    Spinner ui_lang_spin;
    LangSpinnerAdapter _adapter;

    TextView txv_rateNow, txv_remindLater, txv_noThanks, txv_rateUs;

    int _curent_language = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        _curent_language = Preference.getInstance().getValue(this, PrefConst.PREFKEY_CURRNETLANG, 0);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView) findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        txv_name = (TextView)findViewById(R.id.txv_name);

        txv_name.setText(getString(R.string.welcome) + " " + Commons.g_user.get_user_fullname());

        txv_rateUs = (TextView)findViewById(R.id.txv_rate_us);
        txv_rateUs.setOnClickListener(this);

        ui_lang_spin = (Spinner)findViewById(R.id.spinner_lang);
        _adapter = new LangSpinnerAdapter(this);
        ui_lang_spin.setAdapter(_adapter);

        ui_lang_spin.setSelection(_curent_language);

        ui_lang_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0){

                    _curent_language = position;
                    setLanguage("en");
                    overridePendingTransition(R.anim.right_out,R.anim.right_in);

                }else if (position == 1){

                    _curent_language = position;
                    setLanguage("ar");
                    overridePendingTransition(R.anim.right_out,R.anim.right_in);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    protected void onResume() {

        super.onResume();

        Preference.getInstance().put(this, PrefConst.PREFKEY_CURRNETLANG, _curent_language);
        Commons.g_user.set_user_language(String.valueOf(_curent_language));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                gotoMain();
                break;

            case R.id.txv_submit:

                break;

            case R.id.txv_rate_us:
                rateAlert();
                break;
        }

    }
    private void gotoMain(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }

    private void rateAlert(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_alert_rate);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        txv_rateNow = (TextView)dialog.findViewById(R.id.txv_rateNow);
        txv_remindLater = (TextView)dialog.findViewById(R.id.txv_remind_later);
        txv_noThanks = (TextView)dialog.findViewById(R.id.txv_no_thanks);

        txv_rateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        txv_remindLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        txv_noThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }

}
