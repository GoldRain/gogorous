package com.sinmobile.gogorgeos.activity;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.base.CommonActivity;

import java.io.InputStream;

public class TermsActivity extends CommonActivity implements View.OnClickListener{

    ImageView imv_back;
    TextView txv_terms;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        loadLayout();
    }

    private void loadLayout() {

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(this);

        txv_terms = (TextView)findViewById(R.id.txv_terms);

        String result;
        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.terms);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);
        } catch (Exception e) {
            result = "Error : can't show file."  ;
        }

        txv_terms.setText(result.toString());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                overridePendingTransition(0,0);
                break;
        }

    }
}
