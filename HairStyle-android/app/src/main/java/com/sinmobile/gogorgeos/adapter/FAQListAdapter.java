package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.FAQActivity;
import com.sinmobile.gogorgeos.fragment.FAQ.FAQListFragment;
import com.sinmobile.gogorgeos.model.FaqModel;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/19/2017.
 */

public class FAQListAdapter extends BaseAdapter {

    FAQActivity _activity;
    FAQListFragment _faqListFragment;

    ArrayList<FaqModel> _allList = new ArrayList<>();

    public FAQListAdapter (FAQListFragment fragment){

        _activity = (FAQActivity) fragment.getActivity();
        _faqListFragment = fragment;

    }

    public void setList( ArrayList<FaqModel> entity){

        _allList.clear();
        _allList.addAll(entity);
    }

    @Override
    public int getCount() {
        return _allList.size();
    }

    @Override
    public Object getItem(int position) {
        return _allList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        FaqHolder faqHolder;

        if (convertView == null){

            faqHolder = new FaqHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.faq_list_item, parent, false);

            faqHolder.ui_itemTitle = (TextView)convertView.findViewById(R.id.txv_title);
            faqHolder.ui_imvArrow = (ImageView)convertView.findViewById(R.id.imv_arrow);

            convertView.setTag(faqHolder);

        } else {

            faqHolder = (FaqHolder)convertView.getTag();
        }

        final FaqModel faqModel = _allList.get(position);

        faqHolder.ui_itemTitle.setText(faqModel.get_title());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.FaqContentFragment(faqModel);
            }
        });

        return convertView;
    }

    public class FaqHolder{

        TextView ui_itemTitle;
        ImageView ui_imvArrow;
    }
}
