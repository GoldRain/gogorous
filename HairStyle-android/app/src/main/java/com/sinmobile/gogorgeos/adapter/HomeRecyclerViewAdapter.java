package com.sinmobile.gogorgeos.adapter;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.model.UserModel;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 11/16/2016.
 */

public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<HomeRecyclerViewAdapter.ImageHolder>{

    MainActivity _activity;
    ArrayList<UserModel> _users = new ArrayList<>();

    View view ;

    public HomeRecyclerViewAdapter(MainActivity activity , ArrayList<UserModel> users){

        this._activity = activity ;
        this._users = users ;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_home_recycl_image,viewGroup, false);
        ImageHolder viewHolder = new ImageHolder(view);

        viewHolder.lyt_container.getLayoutParams().width = Commons.screenwidth/4;
        int height = Commons.screenwidth/4;
        /*viewHolder.lyt_container.getLayoutParams().height = height*5/4;*/

        viewHolder.imvPhoto.getLayoutParams().width = Commons.screenwidth / 4;
        viewHolder.imvPhoto.getLayoutParams().height = Commons.screenwidth/4;

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageHolder imageHolder = (ImageHolder)v.getTag();

            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImageHolder viewHolder, final int position) {

        UserModel imageHolder = _users.get(position);

        Glide.with(_activity).load(imageHolder.get_user_photoUrl()).placeholder(R.color.news_greay).into(viewHolder.imvPhoto);

        viewHolder.txvTitle.setText(imageHolder.get_user_fullname());

        viewHolder.imvPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserModel user = new UserModel();
                user = _users.get(position);
                //1:artist//2:style//3:model//

                if (Commons.g_user == null){

                    if (user.get_user_role() != 0) {

                        _activity.UserProfile(user);
                    }

                }else if(Commons.g_user.get_user_id() != user.get_user_id()) {

                        if (user.get_user_role() != 0) {

                            _activity.UserProfile(user);

                        }
                    }



            }
        });
    }


    @Override
    public int getItemCount() {

        return (null!= _users ? _users.size():0);
    }

    public class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imvPhoto ;
        TextView txvTitle ;
        LinearLayout lyt_container;

        public ImageHolder(View view) {

            super(view);
            imvPhoto = (ImageView) view.findViewById(R.id.imv_imge_home);
            txvTitle = (TextView)view.findViewById(R.id.txv_name_hom);
            lyt_container = (LinearLayout)view.findViewById(R.id.lyt_container_com);
        }
    }
}
