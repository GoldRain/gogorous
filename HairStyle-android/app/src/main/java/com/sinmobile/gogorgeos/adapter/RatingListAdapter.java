package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.model.RatingModel;
import com.sinmobile.gogorgeos.utils.RadiusImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/20/2017.
 */

public class RatingListAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<RatingModel> _ratings = new ArrayList<>();

    public RatingListAdapter(MainActivity activity, ArrayList<RatingModel> ratings){

        _activity = activity;
        _ratings.clear();
        _ratings = ratings;
    }

    @Override
    public int getCount() {
        return _ratings.size();
        /*return 20;*/
    }

    @Override
    public Object getItem(int position) {
        return _ratings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RatingHolder ratingHolder;

        if (convertView == null){

            ratingHolder = new RatingHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_rating_list, parent, false);

            ratingHolder.imv_photo = (RadiusImageView)convertView.findViewById(R.id.imv_photo_rate);
            ratingHolder.txv_name = (TextView)convertView.findViewById(R.id.txv_Name);
            ratingHolder.txv_comment = (TextView)convertView.findViewById(R.id.txv_comment);
            ratingHolder.rating_num = (SimpleRatingBar)convertView.findViewById(R.id.rating_num);

            convertView.setTag(ratingHolder);

        } else {

            ratingHolder = (RatingHolder)convertView.getTag();
        }

        final RatingModel ratingModel = _ratings.get(position);
        Log.d("Rating_send_name", ratingModel.get_rate_sendername());
        Log.d("Rating_sender_phtoURL", ratingModel.get_rate_senderphoto());

        Glide.with(_activity).load(ratingModel.get_rate_senderphoto()).placeholder(R.drawable.bg_non_profile).into(ratingHolder.imv_photo);

        ratingHolder.txv_comment.setText(ratingModel.get_rate_comment());
        ratingHolder.txv_name.setText(ratingModel.get_rate_sendername());
        ratingHolder.rating_num.setRating(ratingModel.get_rate_marks());


        return convertView;
    }

    public class RatingHolder {

        RadiusImageView imv_photo;
        TextView txv_name;
        TextView txv_comment;
        SimpleRatingBar rating_num;
    }
}
