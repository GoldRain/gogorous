package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;

/**
 * Created by ToSuccess on 12/2/2016.
 */

public class RoleSpinnerAdapter extends BaseAdapter {

    private final Context context;

    String[] RoleState;

    public RoleSpinnerAdapter(Context activity) {
        this.context = activity;

        RoleState = context.getResources().getStringArray(R.array.RoleState);
    }

    @Override
    public int getCount() {
        return RoleState.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_role, parent, false);

        TextView ui_txv_name = (TextView)convertView.findViewById(R.id.txv_state);

    /*    if(position==0){
            ui_txv_name.setText(RoleState[0].trim());
            ui_txv_name.setTextColor(context.getResources().getColor(R.color.hint_color));

        }else {

            ui_txv_name.setText(RoleState[position].trim());
        }*/

        //Constants.publicstatus=ui_txv_name.getText().toString();

        ui_txv_name.setText(RoleState[position].trim());

        return convertView;
    }

   
}
