package com.sinmobile.gogorgeos.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.model.TrainingModel;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/26/2017.
 */

public class TrainRecyclerAdapter extends RecyclerView.Adapter<TrainRecyclerAdapter.ImageHolder> {

    MainActivity _activity;
    ArrayList<TrainingModel> _trainings = new ArrayList<>();

    View view;

    public TrainRecyclerAdapter(MainActivity activity, ArrayList<TrainingModel> trainings){

        _activity = activity;
        _trainings = trainings;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_training_rec, viewGroup, false);

        ImageHolder viewHodler = new ImageHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               ImageHolder imageHolder = (ImageHolder)v.getTag();
            }
        });

        return viewHodler;
    }

    @Override
    public void onBindViewHolder(ImageHolder viewHolder, final int position) {

         final TrainingModel trainingModel = _trainings.get(position);

        viewHolder.lyt_container_tran.getLayoutParams().width = Commons.screenwidth/3;
        int height = Commons.screenwidth/3;
        viewHolder.lyt_container_tran.getLayoutParams().height = height*5/4;

        viewHolder.imvPhoto_tran.getLayoutParams().width = height;

        Glide.with(_activity).load(trainingModel.get_training_imageurl()).placeholder(R.color.news_greay).into(viewHolder.imvPhoto_tran);
        viewHolder.txvTitle_tran.setText(trainingModel.get_training_title());

        viewHolder.lyt_container_tran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.TrainingDetail(trainingModel);
            }
        });
    }


    @Override
    public int getItemCount() {
        return (null!= _trainings ? _trainings.size():0);
    }

    public class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imvPhoto_tran ;
        TextView txvTitle_tran ;
        LinearLayout lyt_container_tran;
        ImageView imvView;

        public ImageHolder(View view) {

            super(view);
            imvPhoto_tran = (ImageView) view.findViewById(R.id.imv_trainImage_t);
            txvTitle_tran = (TextView)view.findViewById(R.id.txv_trainTitle_t);
            lyt_container_tran = (LinearLayout)view.findViewById(R.id.lyt_container_tran_t);
        }
    }
}
