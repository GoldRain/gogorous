package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.model.TrainingModel;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/22/2017.
 */

public class TrainingGridViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<TrainingModel> _allTraining = new ArrayList<>();

    public TrainingGridViewAdapter(MainActivity activity, ArrayList<TrainingModel> trainings){

        _activity = activity;
        _allTraining = trainings;
    }

    @Override
    public int getCount() {
        return _allTraining.size();
    }

    @Override
    public Object getItem(int position) {
        return _allTraining.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        TrainingHolder trainingHolder;

        if (convertView == null){

            trainingHolder = new TrainingHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_training_gdv, parent, false);

            trainingHolder.imv_trainImage = (ImageView)convertView.findViewById(R.id.imv_trainImage);
            trainingHolder.txv_trainTitle = (TextView)convertView.findViewById(R.id.txv_trainTitle);
            trainingHolder.txv_price = (TextView)convertView.findViewById(R.id.txv_price);
            trainingHolder.lyt_container = (LinearLayout)convertView.findViewById(R.id.lyt_container_tg);

            convertView.setTag(trainingHolder);

        } else {

            trainingHolder = (TrainingHolder)convertView.getTag();
        }

        final TrainingModel trainingModel = _allTraining.get(position);

        trainingHolder.lyt_container.getLayoutParams().width = Commons.screenwidth/3;
        int height = Commons.screenwidth/3;
        trainingHolder.lyt_container.getLayoutParams().height = height*5/4;

        Glide.with(_activity).load(trainingModel.get_training_imageurl()).placeholder(R.color.news_greay).into(trainingHolder.imv_trainImage);
        trainingHolder.txv_trainTitle.setText(trainingModel.get_training_title());
        trainingHolder.txv_price.setText("$" + trainingModel.get_training_price());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.TrainingDetail(trainingModel);


            }
        });

        return convertView;
    }

    public class TrainingHolder {

        ImageView imv_trainImage, imv_view;
        TextView txv_trainTitle, txv_price;
        LinearLayout lyt_container;

    }
}
