package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sinmobile.gogorgeos.fragment.UserProfile.UserImageFragment;
import com.sinmobile.gogorgeos.fragment.UserProfile.UserReviewFragment;
import com.sinmobile.gogorgeos.fragment.UserProfile.UserVideoFragment;

/**
 * Created by ToSuccess on 1/21/2017.
 */

public class UserProfileViewPagerAdapter extends FragmentStatePagerAdapter {


    Context _context;
    public UserProfileViewPagerAdapter(FragmentManager fm , Context context) {

        super(fm);
        _context = context;
    }


    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }


    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null ;
        switch (position){
            case 0:

                fragment = new UserImageFragment(_context);
                break;
            case 1:
                fragment = new UserVideoFragment(_context);
                break;
            case 2:
                fragment = new UserReviewFragment(_context);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
