package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.activity.VideoPlayActivity;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.customview.SquareImageView;
import com.sinmobile.gogorgeos.model.VideoModel;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/20/2017.
 */

public class VideoGridViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<VideoModel> _thumbs = new ArrayList<>();
    ArrayList<VideoModel> _allThumbs = new ArrayList<>();

    public VideoGridViewAdapter(MainActivity activity ){

        _activity = activity;

    }

    public void setVideo (ArrayList<VideoModel> thumbs){

        _allThumbs = thumbs;
        _thumbs.clear();
        _thumbs.addAll(_allThumbs);

        notifyDataSetChanged();

    }

    @Override
    public int getCount() {
        return _thumbs.size();
    }

    @Override
    public Object getItem(int position) {
        return _thumbs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        VideoHolder videoHolder;

        if (convertView == null){

            videoHolder = new VideoHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_video_gridview, parent, false);

            videoHolder.imv_thumb = (SquareImageView)convertView.findViewById(R.id.imv_thumb);
            videoHolder.imv_play = (ImageView)convertView.findViewById(R.id.imv_play);

            convertView.setTag(videoHolder);

        } else {

            videoHolder = (VideoHolder)convertView.getTag();
        }

        final VideoModel videoEntity = _thumbs.get(position);

        Glide.with(_activity).load(videoEntity.get_video_thumbimageurl()).placeholder(R.drawable.images_1).into(videoHolder.imv_thumb);

        videoHolder.imv_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String path = videoEntity.get_video_url();

                Intent intent = new Intent(_activity, VideoPlayActivity.class);
                intent.putExtra(Constants.KEY_VIDEOPATH, path);

                Log.d("==video====>", path);
                //_activity.startActivityForResult(intent, Constants.PICK_FROM_VIDEO);
                _activity.startActivity(intent);

               // _activity.showToast("Play");
            }
        });

        return convertView;
    }

    public class VideoHolder {

        SquareImageView imv_thumb;
        ImageView imv_play;
    }
}
