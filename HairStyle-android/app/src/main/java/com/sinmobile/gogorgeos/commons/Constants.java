package com.sinmobile.gogorgeos.commons;

/**
 * Created by GoldRain on 9/25/2016.
 */
public class Constants {

    public static final int VOLLEY_TIME_OUT = 10000;
    public static final int SPLASH_TIME = 500;
    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final String KEY_LOGOUT = "logout";
    public static final String USER = "user";
    public static final String CLASS = "class";
    public static final String KEY_ROOM = "room";


    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;

    public static final int MY_PEQUEST_CODE = 104;
    public static final int PICK_FROM_VIDEO = 105;
    public static final int PICK_FROM_VIDEO_GALLERY = 106;

    public static  boolean ARTIST = false;
    public static  boolean STYLE = false ;
    public static  boolean MODEL = false;

    public static final String KEY_IMAGEPATH = "image_path";
    public static final String KEY_POSITION = "position";
    public static final String KEY_VIDEOPATH = "video_path";
    public static final String KEY_LOCATION = "location";

    public static String token = "";
    public static String receivemessage="";

}
