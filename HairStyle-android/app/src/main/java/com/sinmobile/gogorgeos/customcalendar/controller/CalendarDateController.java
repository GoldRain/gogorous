package com.sinmobile.gogorgeos.customcalendar.controller;

import android.util.Log;

import com.sinmobile.gogorgeos.customcalendar.data.CalendarDate;
import com.sinmobile.gogorgeos.customcalendar.data.Solar;
import com.sinmobile.gogorgeos.customcalendar.utils.CalendarUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.lang.Math.pow;


/**
 * Created by joybar on 2/24/16.
 */
public class CalendarDateController {

    public static List<CalendarDate> getCalendarDate(int year, int month, int day) {
        List<CalendarDate> mListDate = new ArrayList<>();
        List<CalendarUtils.CalendarSimpleDate> list = null;
        try {
            list = CalendarUtils.getEverydayOfMonth(year, month, day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int count = list.size();

        for (int i = 0; i < count; i++) {
            Solar solar = new  Solar();
            solar.solarYear = list.get(i).getYear();
            solar.solarMonth = list.get(i).getMonth();
            solar.solarDay = list.get(i).getDay();

            //Lunar lunar = LunarSolarConverter.SolarToLunar(solar);
            mListDate.add(new CalendarDate( month == list.get(i).getMonth(),day == list.get(i).getDay(),  false, solar));
        }

        return mListDate;
    }

    public static List<CalendarDate> getCustomCalendarDate(List<Long> valueArray, int year, int month){

        List<CalendarDate> mListDate = new ArrayList<>();
        List<CalendarUtils.CalendarSimpleDate> list = null;
        try {
            list = CalendarUtils.getEverydayOfMonth(year, month, 0);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int count = list.size();

        for (int i = 0; i < count; i++) {
            Solar solar = new  Solar();
            solar.solarYear = list.get(i).getYear();
            solar.solarMonth = list.get(i).getMonth();
            solar.solarDay = list.get(i).getDay();

            //Lunar lunar = LunarSolarConverter.SolarToLunar(solar);

            if(((long)pow((double)2,(double)solar.solarDay) & valueArray.get(solar.solarMonth - 1)) == (long)pow((double)2,(double)solar.solarDay) ){
                mListDate.add(new CalendarDate( month == list.get(i).getMonth(),true,  true, solar));
            }
            else{
                mListDate.add(new CalendarDate( month == list.get(i).getMonth(),false,  false, solar));
            }
        }

        return mListDate;
    }


}
