package com.sinmobile.gogorgeos.customcalendar.data;

/**
 * Created by joybar on 2/24/16.
 */
public class CalendarDate {

    private Lunar lunar = new Lunar();//农历
    private Solar solar = new Solar();//公历
    private boolean isInThisMonth; //是否在当月
    private boolean isSelect;//是否被选中
    private boolean isInThisDay;

    public CalendarDate(int year, int month, int day, boolean isInThisMonth, boolean isInThisDay, boolean isSelect) {
        this.isInThisMonth = isInThisMonth;
        this.isSelect = isSelect;
        this.isInThisDay = isInThisDay;
        this.lunar = lunar;
    }


    public CalendarDate(boolean isInThisMonth, boolean isInThisDay, boolean isSelect, Solar solar) {
        this.isInThisMonth = isInThisMonth;
        this.isInThisDay = isInThisDay;
        this.isSelect = isSelect;
        this.solar = solar;
        this.lunar = lunar;
    }

    public boolean isInThisMonth() {
        return isInThisMonth;
    }


    public void setIsInThisMonth(boolean isInThisMonth) {
        this.isInThisMonth = isInThisMonth;
    }

    public boolean isInthisDay(){

        return isInThisDay;
    }

    public void setInthisDay(boolean isInthisDay){

        this.isInThisDay = isInthisDay;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setIsSelect(boolean isSelect) {
        this.isSelect = isSelect;
    }

    public Solar getSolar() {
        return solar;
    }

    public void setSolar(Solar solar) {
        this.solar = solar;
    }

    public void setInThisMonth(boolean inThisMonth) {
        isInThisMonth = inThisMonth;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public Lunar getLunar() {
        return lunar;
    }

    public void setLunar(Lunar lunar) {
        this.lunar = lunar;
    }
}
