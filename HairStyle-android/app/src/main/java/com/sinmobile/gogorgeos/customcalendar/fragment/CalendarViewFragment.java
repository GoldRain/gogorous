package com.sinmobile.gogorgeos.customcalendar.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.customcalendar.adapter.CalendarGridViewAdapter;
import com.sinmobile.gogorgeos.customcalendar.controller.CalendarDateController;
import com.sinmobile.gogorgeos.customcalendar.data.CalendarDate;
import com.sinmobile.gogorgeos.customcalendar.utils.DateUtils;
import com.sinmobile.gogorgeos.fragment.MyProfile.MyProfileEditFragment;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.utils.MonthDays;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by joybar on 4/27/16.
 */
public class CalendarViewFragment extends Fragment {

    private static final String YEAR = "year";
    private static final String MONTH = "month";
    private static final String CHOICE_MODE_SINGLE = "choice_mode_single";
    private boolean isChoiceModelSingle;
    private int mYear;
    private int mMonth;
    private int  mDay;
    private  GridView mGridView;
    private OnDateClickListener onDateClickListener;
    private OnDateCancelListener onDateCancelListener;

    List<Long> _valuesArray = new ArrayList<>();

    MonthDays month_days = new MonthDays();

    MyProfileEditFragment _fragment = new MyProfileEditFragment();

    public CalendarViewFragment() {
    }

    public static CalendarViewFragment newInstance(int year, int month) {
        CalendarViewFragment fragment = new CalendarViewFragment();
        Bundle args = new Bundle();
        args.putInt(YEAR, year);
        args.putInt(MONTH, month);
        fragment.setArguments(args);
        return fragment;
    }

    public static CalendarViewFragment newInstance(int year, int month, boolean isChoiceModelSingle, List<Long> valuesArray) {
        CalendarViewFragment fragment = new CalendarViewFragment();
        Bundle args = new Bundle();
        args.putInt(YEAR, year);
        args.putInt(MONTH, month);
        args.putBoolean(CHOICE_MODE_SINGLE, isChoiceModelSingle);
        fragment.setArguments(args);

        fragment._valuesArray = valuesArray;

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onDateClickListener = (OnDateClickListener) context;
            if(!isChoiceModelSingle){
                //多选
                onDateCancelListener = (OnDateCancelListener) context;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement OnDateClickListener or OnDateCancelListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mYear = getArguments().getInt(YEAR);
            mMonth = getArguments().getInt(MONTH);
            isChoiceModelSingle = getArguments().getBoolean(CHOICE_MODE_SINGLE, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        mGridView = (GridView) view.findViewById(R.id.gv_calendar);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        final List<CalendarDate> mListDataCalendar;

        if (_valuesArray.size() == 0){

            mListDataCalendar = CalendarDateController.getCalendarDate(mYear, mMonth, mDay);//日历数据

        }else mListDataCalendar = CalendarDateController.getCustomCalendarDate(_valuesArray, mYear, mMonth);//日历数据

        mGridView.setAdapter(new CalendarGridViewAdapter(mListDataCalendar));
       // Commons.listdatacalenda.clear();
       // Commons.listdatacalenda=mListDataCalendar;

        final List<CalendarDate> finalMListDataCalendar = mListDataCalendar;

        if (isChoiceModelSingle) {
            mGridView.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
        } else {
            mGridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
        }

        if (Commons.MY_PROFILE == 1){


        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CalendarDate calendarDate = ((CalendarGridViewAdapter) mGridView.getAdapter()).getListData().get(position);
                if (isChoiceModelSingle) {
                    //单选
                    if (finalMListDataCalendar.get(position).isInThisMonth()) {
                        onDateClickListener.onDateClick(calendarDate);
                    } else {
                        mGridView.setItemChecked(position, false);

                    }

                } else {
                    //多选
                    if (finalMListDataCalendar.get(position).isInThisMonth()) {

                       if(mListDataCalendar.get(position).isSelect()==true){

                           mListDataCalendar.get(position).setIsSelect(false);
                           long day = month_days.removeDayFromMonthValue((Integer) calendarDate.getSolar().solarDay, _valuesArray.get(calendarDate.getSolar().solarMonth - 1));
                           _valuesArray.set(calendarDate.getSolar().solarMonth - 1, day);

                       }else {

                           mListDataCalendar.get(position).setIsSelect(true);

                           long day = month_days.addDayToMonthValue((Integer) calendarDate.getSolar().solarDay , _valuesArray.get(calendarDate.getSolar().solarMonth -1));
                           _valuesArray.set(calendarDate.getSolar().solarMonth -1, day);

                       }



                        Commons.valuesArray = _valuesArray;
                        //_fragment.allAvailable((ArrayList<Long>) Commons.valuesArray);


                        if(!mGridView.isItemChecked(position)){
                            onDateCancelListener.onDateCancel(calendarDate);

                        } else {
                            onDateClickListener.onDateClick(calendarDate);
                        }

                    } else {

                        mGridView.setItemChecked(position, false);
                    }
                    Log.d("change_value===>", String.valueOf(Commons.valuesArray));
                }

            }
        });

        }

        mGridView.post(new Runnable() {
            @Override
            public void run() {
                //需要默认选中当天
                List<CalendarDate> mListData = ((CalendarGridViewAdapter) mGridView.getAdapter()).getListData();
                int count = mListData.size();
                for (int i = 0; i < count; i++) {
                    if (mListData.get(i).getSolar().solarDay == DateUtils.getDay()
                            && mListData.get(i).getSolar().solarMonth == DateUtils.getMonth()
                            && mListData.get(i).getSolar().solarYear == DateUtils.getYear()) {
                        if (null != mGridView.getChildAt(i) && mListData.get(i).isInThisMonth()) {
                            // mListData.get(i).setIsSelect(true);
                            onDateClickListener.onDateClick(mListData.get(i));
                            mGridView.setItemChecked(i, true);
                        }
                    }
                }

            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            if (null != mGridView) {
                // mGridView.setItemChecked(mCurrentPosition, false);
                mGridView.clearChoices();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface OnDateClickListener {
        void onDateClick(CalendarDate calendarDate);
    }
    public interface OnDateCancelListener {
        void onDateCancel(CalendarDate calendarDate);
    }
}
