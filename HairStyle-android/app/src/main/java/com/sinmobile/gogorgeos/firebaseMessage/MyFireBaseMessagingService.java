package com.sinmobile.gogorgeos.firebaseMessage;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aa on 10/14/2016.
 */
public class MyFireBaseMessagingService extends FirebaseMessagingService {

    MainActivity activity;
    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Log.d("Tiltel==>", remoteMessage.getNotification().getTitle());
            Constants.receivemessage = remoteMessage.getNotification().getBody();
        }
        if (remoteMessage.getNotification() != null) {
            //Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

         //   Log.d(TAG,"======Message Notification Sound" + remoteMessage.getNotification().getSound());

          //  Log.d("mesagesount==========",remoteMessage.getNotification().getSound());

            String sound = remoteMessage.getNotification().getSound();

            if (sound.length() > 0){

                parseNoti(sound);
                Log.d("Notification==", sound);

            } else Toast.makeText(this, "Failed Noti", Toast.LENGTH_SHORT).show();



            //String body = remoteMessage.getNotification().getBody();

            /*String[] separated = sound.split(" : ");

            ArrayList aList = new ArrayList(Arrays.asList(sound.split(":")));

           // Log.d("messagetype====",aList.get(3).toString().trim());

            if (aList.size() > 0) {

                for (int i = 0; i < aList.size(); i++) {

            *//*    if(aList.get(1).toString().trim().equals("1")){

                    Constants.messagetype = 0;
                    //Constants.messagestatuse=false;
                    Constants.recieveimageurl = remoteMessage.getNotification().getBody();
                    Log.d("imageurl========",Constants.recieveimageurl);
                    }*//*

                    Constants.noti_counter = aList.size();

                    Log.d("=====noti_counter=====", String.valueOf(Constants.noti_counter));

                    Constants.messagetype = 0;

          *//*          Log.d("sfsdfsd","sfsdfsd");
                    Constants.messagetype = 0;
                    Constants.messagestatuse = true;
                    Constants.receivemessage = remoteMessage.getNotification().getBody();*//*

                    NotiModel notiModel = new NotiModel();

                    notiModel.setId(Integer.parseInt(aList.get(1).toString()));
                    notiModel.setNoti_type(Integer.parseInt(aList.get(3).toString().trim()));
                    notiModel.setUsername(aList.get(5).toString().trim());
                    String photo_url = aList.get(7).toString().trim();
                    photo_url = photo_url.replace("%999",".");
                    notiModel.setNoti_photo(photo_url);

                    Constants.notiModels.add(notiModel);

                }

            } else {

                Toast.makeText(this, "Null", Toast.LENGTH_SHORT).show();
            }*/

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

    }

    private void parseNoti(String json){

        /*Constants.noti_counter++ ;

        try {

            JSONObject response = new JSONObject(json);

            NotiModel notiModel = new NotiModel() ;

            notiModel.setId(response.getInt("sender_id"));
            notiModel.setNoti_type(response.getInt("message_type"));
            notiModel.setUsername(response.getString("username"));

            String photo_url = response.getString(ReqConst.RES_PHOTO_URL);
            photo_url = photo_url.replace("%999", ".");
            notiModel.setNoti_photo(photo_url);

            Log.d("======MessageCouter", String.valueOf(Constants.noti_counter));
            Log.d("=====Id ===", String.valueOf(notiModel.getId()));
            Log.d("=====messagetype ===", String.valueOf(notiModel.getNoti_type()));
            Log.d("=====Id ===", notiModel.getUsername());
            Log.d("=====Id ===", (notiModel.getNoti_photo()));

            Constants.notiModels.add(notiModel);

            showNotification("You received a request from friend !");

        } catch (JSONException e) {
            e.printStackTrace();
        }*/

    }

    private void showNotification(String msg) {

        //Constants.noti_status = 1;

        Intent intent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(msg)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 , notificationBuilder.build());

        //---------------------
        Intent _intent = new Intent("my-event");
        LocalBroadcastManager.getInstance(this).sendBroadcast(_intent);

    }
}
