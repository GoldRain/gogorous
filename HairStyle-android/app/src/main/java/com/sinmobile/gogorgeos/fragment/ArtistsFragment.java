package com.sinmobile.gogorgeos.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.UserRoleGridViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.AvailableModel;
import com.sinmobile.gogorgeos.model.RatingModel;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.model.VideoModel;
import com.sinmobile.gogorgeos.parses.RoleModelPaser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ArtistsFragment extends Fragment {

    MainActivity _activity;
    View view;
    ImageView ui_imvBack;
    GridView grv_artist;
    UserRoleGridViewAdapter _adapter;
    ArrayList<UserModel> _artists = new ArrayList<>();

    public ArtistsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_artists, container, false);

        Commons.ROLE_STATE = 1;

        if (Commons.SEARCH_PARAM == 1){
            getfromName();

        }else if (Commons.SEARCH_PARAM == 2){
            getfromDate();

        }else if (Commons.SEARCH_PARAM == 3){
            getfromAddress();

        } else  getArtists();

        return view;
    }

    private void getfromName(){

        _activity.initParam(0);

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCH_NANE;

        Log.d("URL++ARtist", url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseArtist(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USERROLE, String.valueOf(1));
                    params.put(ReqConst.PARAM_NAME, Commons.SEARCH_EDIT);

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void getfromDate(){

        _activity.initParam(0);

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCH_DATE;

        Log.d("URL++ARDATA", url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseArtist(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USERROLE, String.valueOf(1));
                    params.put(ReqConst.PARAM_YEAR, String.valueOf(Commons.YEAR));
                    params.put(ReqConst.PARAM_MONTH, Commons.MONTH);
                    params.put(ReqConst.PARAM_VALUE, Commons.VALUE);
                    params.put(ReqConst.PARAM_ADDRESS, Commons.SEARCH_EDIT);

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);


    }

    private void getfromAddress(){

        _activity.initParam(0);


        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCH_ADDRESS;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseArtist(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USERROLE, String.valueOf(1));
                    params.put(ReqConst.PARAM_ADDRESS, Commons.SEARCH_EDIT);

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void getArtists(){

        _activity.initParam(0);

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETUSERS;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseArtist(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USERROLE, String.valueOf(1));

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseArtist(String response){

        Log.d("====Response_Artist==?", response);

        try {
            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);

            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                _activity.closeProgress();
               _artists.clear();

                JSONArray artists = object.getJSONArray(ReqConst.RES_USERS);
                for (int i = 0; i < artists.length(); i++){

                    JSONObject artist_object = (JSONObject)artists.get(i);
                    RoleModelPaser artists_role = new RoleModelPaser();

                    artists_role.parseJson(artist_object);

                   while (!artists_role.is_isFinish()){}

                    artists_role.get_user();

                    _artists.add(artists_role.get_user());
                }
                loadLayout();

            } else {
                _activity.closeProgress();
                _activity.showAlertDialog(result_message);
            }

        } catch (JSONException e) {

            _activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void loadLayout(){


        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Commons.ROLE_STATE = 0;
                _activity.onBackPressed();
            }
        });

        grv_artist = (GridView)view.findViewById(R.id.gdv_artist);
        _adapter = new UserRoleGridViewAdapter(_activity, _artists);
        grv_artist.setAdapter(_adapter);

        _adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

}
