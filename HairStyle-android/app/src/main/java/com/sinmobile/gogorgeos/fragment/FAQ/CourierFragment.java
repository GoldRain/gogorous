package com.sinmobile.gogorgeos.fragment.FAQ;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.FAQActivity;

public class CourierFragment extends Fragment {

    FAQActivity _activity;
    View view;

    public CourierFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_courier, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (FAQActivity) context;
    }
}
