package com.sinmobile.gogorgeos.fragment.FAQ;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.FAQActivity;
import com.sinmobile.gogorgeos.adapter.FAQListAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.model.FaqModel;

import java.util.ArrayList;

public class FAQListFragment extends Fragment implements View.OnClickListener{

    FAQActivity _activity;
    View view;
    ListView ui_faqList;
    FAQListAdapter _adapter;

    ArrayList<FaqModel> _entity = new ArrayList<>();
    //LinearLayout ui_lytAccount, ui_lytShopping, ui_lytSell, ui_lytPayment, ui_lytGift, ui_lytOrder, ui_shipping, ui_lytCourier;

    public FAQListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_faqlist, container, false);

        loadLayout();

        loadTitle();

        return view;
    }

    public void loadTitle(){

        _adapter.setList(Commons.FAQS);
        ui_faqList.setAdapter(_adapter);

       // _adapter.notifyDataSetChanged();

    }

    private void loadLayout() {

        _adapter = new FAQListAdapter(this);
        ui_faqList =  (ListView)view.findViewById(R.id.lst_faqList);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (FAQActivity) context;
    }

}
