package com.sinmobile.gogorgeos.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.customcalendar.fragment.CalendarViewPagerFragment2;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class MyAlertDialogFragment extends DialogFragment implements CalendarViewPagerFragment2.OnPageChangeListener {

    public static final String TITLE = "dataKey";
    List<Long> _valuesArray = new ArrayList<>();
    boolean isChoiceModelSingle = false;


    TextView ui_txvDate;

    public static MyAlertDialogFragment newInstance(boolean single, List<Long> valuesArray) {

        MyAlertDialogFragment yourDialogFragment = new MyAlertDialogFragment();
        yourDialogFragment._valuesArray = valuesArray;
        yourDialogFragment.isChoiceModelSingle = single;

        return yourDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.custom_view_calendar, null);

        ui_txvDate = (TextView) view.findViewById(R.id.tv_date);

        ui_txvDate.setText(Calendar.getInstance().get(Calendar.YEAR) + "-" + (Calendar.getInstance().get(Calendar.MONTH)+1));


        CalendarViewPagerFragment2 fragment = CalendarViewPagerFragment2.newInstance(isChoiceModelSingle, _valuesArray );
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.f2_content1, fragment);
        fragmentTransaction.commit();
        fragment.setOnPageChangeListener(this);

        return view;
    }

    public void onPageChange(int year, int month) {

        ui_txvDate.setText(year + "-" + month);
    }
}
