package com.sinmobile.gogorgeos.fragment.News;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.NewsGridViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.NewsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.sinmobile.gogorgeos.commons.Commons.LANGUAGE;

public class NewsListsFragment extends Fragment {

    MainActivity _activity;
    View view;
    ImageView ui_imvBack;

    ArrayList<NewsModel> _newsModels = new ArrayList<>();
    GridView gdv_news;
    NewsGridViewAdapter _adapter_news;

    public NewsListsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_news, container, false);

        Commons.ROLE_STATE = 4;
        getNews();
        return view;
    }

    private void getNews(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETNEWS;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseNews(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_USER_LANGUAGE, String.valueOf(LANGUAGE));

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(_activity.getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseNews(String response){

        try {
            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);
            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                JSONArray news_model = object.getJSONArray(ReqConst.RES_NEWS);
                for (int i = 0; i < news_model.length(); i++){

                    JSONObject news_json = (JSONObject)news_model.get(i);

                    NewsModel news = new NewsModel();

                    news.set_news_id(news_json.getInt(ReqConst.PARAM_NEWSID));
                    news.set_news_title(news_json.getString(ReqConst.PARAM_NEWSTITLE));
                    news.set_news_date(news_json.getString(ReqConst.PARAM_NEWSDATE));
                    news.set_news_imageurl(news_json.getString(ReqConst.PARAM_NEWSIMAGEURL));
                    news.set_news_content(news_json.getString(ReqConst.PARAM_NEWSCONTENT));

                    _newsModels.add(news);
                }

                loadLayout();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadLayout(){

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commons.ROLE_STATE = 0;
                _activity.onBackPressed();
            }
        });

        gdv_news = (GridView)view.findViewById(R.id.gdv_news);
        _adapter_news = new NewsGridViewAdapter(_activity, _newsModels);
        gdv_news.setAdapter(_adapter_news);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}
