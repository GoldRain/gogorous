package com.sinmobile.gogorgeos.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.UserRoleGridViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.parses.RoleModelPaser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StylistsFragment extends Fragment {

    MainActivity _activity_style;
    View view;
    ImageView ui_imvBack;

    GridView grv_style;
    UserRoleGridViewAdapter _adapter_style;

    ArrayList<UserModel> _styles = new ArrayList<>();

    String url = "";

    public StylistsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_stylists, container, false);


        Commons.ROLE_STATE = 2;
        //loadLayout();
        getStyles();
        return view;
    }

    private void getStyles(){

        Log.d("**********Param***", String.valueOf(Commons.SEARCH_PARAM));


        _activity_style.showProgress();

        if (Commons.SEARCH_PARAM == 1){

            url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCH_NANE;

        }else if (Commons.SEARCH_PARAM == 2){

            url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCH_DATE;

        }else if (Commons.SEARCH_PARAM == 3){

            url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCH_ADDRESS;

        } else  url = ReqConst.SERVER_URL + ReqConst.REQ_GETUSERS;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseStyles(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity_style.closeProgress();
                _activity_style.showAlertDialog(_activity_style.getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    if (Commons.SEARCH_PARAM == 1){

                        _activity_style.initParam(0);

                        params.put(ReqConst.PARAM_NAME, Commons.SEARCH_EDIT);

                    }else if (Commons.SEARCH_PARAM == 2){
                        _activity_style.initParam(0);

                        params.put(ReqConst.PARAM_YEAR, String.valueOf(Commons.YEAR));
                        params.put(ReqConst.PARAM_MONTH, Commons.MONTH);
                        params.put(ReqConst.PARAM_VALUE, Commons.VALUE);
                        params.put(ReqConst.PARAM_ADDRESS, Commons.SEARCH_EDIT);

                    }else if (Commons.SEARCH_PARAM == 3){
                        _activity_style.initParam(0);
                        params.put(ReqConst.PARAM_ADDRESS, Commons.SEARCH_EDIT);}

                    _activity_style.initParam(0);

                    params.put(ReqConst.PARAM_USERROLE, String.valueOf(2));

                } catch (Exception e) {

                    _activity_style.closeProgress();
                    _activity_style.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseStyles(String response){

        Log.d("====Response_Artist==?", response);

        try {
            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);

            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                _activity_style.closeProgress();

                JSONArray styles = object.getJSONArray(ReqConst.RES_USERS);

                for (int i = 0; i < styles.length(); i++){

                    JSONObject json_model = styles.getJSONObject(i);

                    RoleModelPaser style_role = new RoleModelPaser();

                    style_role.parseJson(json_model);

                    while (!style_role.is_isFinish()){}

                    _styles.add(style_role.get_user());

                }

                loadLayout();

            } else {
                _activity_style.closeProgress();
                _activity_style.showAlertDialog(result_message);
            }

        } catch (JSONException e) {

            _activity_style.closeProgress();
            _activity_style.showAlertDialog(_activity_style.getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void loadLayout(){

        ui_imvBack = (ImageView)_activity_style.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commons.ROLE_STATE = 0;
                _activity_style.onBackPressed();
            }
        });

        grv_style = (GridView)view.findViewById(R.id.gdv_style);
        _adapter_style = new UserRoleGridViewAdapter(_activity_style, _styles);
        grv_style.setAdapter(_adapter_style);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity_style = (MainActivity) context;
    }


}
