package com.sinmobile.gogorgeos.fragment.Training;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.TrainingGridViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.TrainingModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.sinmobile.gogorgeos.commons.Commons.LANGUAGE;

public class TrainingFragment extends Fragment {

    MainActivity _activity;
    View view;
    ImageView ui_imvBack;
    GridView gdv_train;
    TrainingGridViewAdapter _adapter_train;
    ArrayList<TrainingModel> _allTrain = new ArrayList<>();

    public TrainingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_training, container, false);
        Commons.ROLE_STATE = 5;

        getTraining();

        return view;
    }

    private void getTraining(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETTRAINING;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseTraining(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_USER_LANGUAGE, String.valueOf(LANGUAGE));

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(_activity.getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseTraining(String response){

        Log.d("===TRAINING+++", response);

        try {
            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);
            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                JSONArray train_model = object.getJSONArray(ReqConst.RES_TRAININGS);
                for (int i = 0; i < train_model.length(); i++){

                    JSONObject training_json = (JSONObject)train_model.get(i);

                    TrainingModel training = new TrainingModel();

                    training.set_training_id(training_json.getInt(ReqConst.PARAM_TRAINID));
                    training.set_training_title(training_json.getString(ReqConst.PARAM_TRAINTITLE));
                    training.set_training_date(training_json.getString(ReqConst.PARAM_TRAINDATE));
                    training.set_training_location(training_json.getString(ReqConst.PARAM_TRAINLOCATION));
                    training.set_training_email(training_json.getString(ReqConst.PARAM_TRAINEMAIL));
                    training.set_training_imageurl(training_json.getString(ReqConst.PARAM_TRAINIMAGEURL));
                    training.set_training_content(training_json.getString(ReqConst.PARAM_TRAINCONTENT));
                    training.set_training_price(training_json.getDouble(ReqConst.PARAM_TRAINPRICE));
                    training.set_training_phonenumber(training_json.getString(ReqConst.PARAM_TRAIN_PHONENUMBER));

                    _allTrain.add(training);
                }

                loadLayout();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadLayout(){

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commons.ROLE_STATE = 0;
                _activity.onBackPressed();
            }
        });

        gdv_train = (GridView)view.findViewById(R.id.gdv_train);
        _adapter_train = new TrainingGridViewAdapter(_activity, _allTrain);
        gdv_train.setAdapter(_adapter_train);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}
