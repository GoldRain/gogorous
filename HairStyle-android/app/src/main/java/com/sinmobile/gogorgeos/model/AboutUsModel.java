package com.sinmobile.gogorgeos.model;

/**
 * Created by HugeRain on 2/18/2017.
 */

public class AboutUsModel {

    int _id = 0;
    String _title = "";
    String _content = "";

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_content() {
        return _content;
    }

    public void set_content(String _content) {
        this._content = _content;
    }
}
