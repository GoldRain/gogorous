package com.sinmobile.gogorgeos.model;

import java.io.Serializable;

/**
 * Created by ToSuccess on 1/21/2017.
 */

public class ArtistEntity implements Serializable {

    int _id_artist  = 0;
    String _photo_artist = "";
    String _name_artist = "";

    public int get_id_artist() {
        return _id_artist;
    }

    public void set_id_artist(int _id_artist) {
        this._id_artist = _id_artist;
    }

    public String get_photo_artist() {
        return _photo_artist;
    }

    public void set_photo_artist(String _photo_artist) {
        this._photo_artist = _photo_artist;
    }

    public String get_name_artist() {
        return _name_artist;
    }

    public void set_name_artist(String _name_artist) {
        this._name_artist = _name_artist;
    }
}
