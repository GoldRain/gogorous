package com.sinmobile.gogorgeos.model;

/**
 * Created by ToSuccess on 2/6/2017.
 */

public class AvailableModel {

    int _year = 0;
    int _month = 0;
    long _value = 0;


    public int get_year() {
        return _year;
    }

    public void set_year(int _year) {
        this._year = _year;
    }

    public int get_month() {
        return _month;
    }

    public void set_month(int _month) {
        this._month = _month;
    }

    public long get_value() {
        return _value;
    }

    public void set_value(long _value) {
        this._value = _value;
    }
}
