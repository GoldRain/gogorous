package com.sinmobile.gogorgeos.model;

import java.io.Serializable;

/**
 * Created by ToSuccess on 1/22/2017.
 */

public class NewsModel  implements Serializable{

    int _news_id = 0;
    String _news_title = "";
    String _news_date = "";
    String _news_imageurl = "";
    String _news_content = "";

    public int get_news_id() {
        return _news_id;
    }

    public void set_news_id(int _news_id) {
        this._news_id = _news_id;
    }

    public String get_news_title() {
        return _news_title;
    }

    public void set_news_title(String _news_title) {
        this._news_title = _news_title;
    }

    public String get_news_date() {
        return _news_date;
    }

    public void set_news_date(String _news_date) {
        this._news_date = _news_date;
    }

    public String get_news_imageurl() {
        return _news_imageurl;
    }

    public void set_news_imageurl(String _news_imageurl) {
        this._news_imageurl = _news_imageurl;
    }

    public String get_news_content() {
        return _news_content;
    }

    public void set_news_content(String _news_content) {
        this._news_content = _news_content;
    }
}
