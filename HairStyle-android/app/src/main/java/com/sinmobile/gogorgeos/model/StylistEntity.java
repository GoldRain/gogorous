package com.sinmobile.gogorgeos.model;

/**
 * Created by ToSuccess on 1/21/2017.
 */

public class StylistEntity {

    int _id_style  = 0;
    String _photo_style = "";
    String _name_style = "";

    public int get_id_style() {
        return _id_style;
    }

    public void set_id_style(int _id_style) {
        this._id_style = _id_style;
    }

    public String get_photo_style() {
        return _photo_style;
    }

    public void set_photo_style(String _photo_style) {
        this._photo_style = _photo_style;
    }

    public String get_name_style() {
        return _name_style;
    }

    public void set_name_style(String _name_style) {
        this._name_style = _name_style;
    }
}
