package com.sinmobile.gogorgeos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by ToSuccess on 1/20/2017.
 */

public class UserModel implements Serializable {

    int _user_id = 0;

    String _user_firstname = "";
    String _user_lastname = "";
    String _user_email = "";
    String _user_password = "";
    int _user_gender = 0;         //values USER_GENDER_DEFAULT : 0, USER_GENDER_MALE: 1 , USER_GENDER_FEMALE: 2
    String _user_birthday = "";
    int _user_subscribed = 0;    //send news to email : 1, don't send : 1 : if it checked, user can receive news emails from server
    int _user_role = 0;          //0: normal: 1: artist, 2: stylist, 3: Models, 0 : not defined

    //==============================

    ArrayList<String> _user_images = new ArrayList<>();
    ArrayList<RatingModel> _user_ratings = new ArrayList<>();
    ArrayList<VideoModel> _user_video = new ArrayList<>();

    String _user_phonenumber = "";
    String _user_adress = "";
    String _user_fblink = "";
    String _user_twlink = "";
    String _user_yolink = "";
    String _user_inlink = "";
    String _user_snlink = "";

    String _user_photoUrl = "";
    String _user_aboutme = "";

    String _user_verificationcode = "";                //otp verification:
    String _user_language = "";

    ArrayList<AvailableModel> _user_available = new ArrayList<>();  //[{year, month, value}]

    String _serve_location = "";

    /*//////////////////////////////////// START //////////////////////////////////////////*/

    public int get_user_id() {
        return _user_id;
    }

    public void set_user_id(int _user_id) {
        this._user_id = _user_id;
    }

    public int get_user_gender() {
        return _user_gender;
    }

    public void set_user_gender(int _user_gender) {
        this._user_gender = _user_gender;
    }

    public int get_user_role() {
        return _user_role;
    }

    public void set_user_role(int _user_role) {
        this._user_role = _user_role;
    }

    public String get_user_firstname() {
        return _user_firstname;
    }

    public void set_user_firstname(String _user_firstname) {
        this._user_firstname = _user_firstname;
    }

    public String get_user_lastname() {
        return _user_lastname;
    }

    public void set_user_lastname(String _user_lastname) {
        this._user_lastname = _user_lastname;
    }

    public String get_user_fullname(){return _user_firstname + _user_lastname;
    }

    public String get_user_email() {
        return _user_email;
    }

    public void set_user_email(String _user_email) {
        this._user_email = _user_email;
    }

    public String get_user_password() {
        return _user_password;
    }

    public void set_user_password(String _user_password) {
        this._user_password = _user_password;
    }

    public String get_user_photoUrl() {
        return _user_photoUrl;
    }

    public void set_user_photoUrl(String _user_photoUrl) {
        this._user_photoUrl = _user_photoUrl;
    }

    public ArrayList<String> get_user_images() {
        return _user_images;
    }

    public void set_user_images(ArrayList<String> _images) {
        this._user_images = _images;
    }

    public String get_user_adress() {
        return _user_adress;
    }

    public void set_user_adress(String _user_adress) {
        this._user_adress = _user_adress;
    }

    public String get_user_phonenumber() {
        return _user_phonenumber;
    }

    public void set_user_phonenumber(String _user_phonenumber) {
        this._user_phonenumber = _user_phonenumber;
    }

    public ArrayList<RatingModel> get_user_ratings() {
        return _user_ratings;
    }

    public void set_user_ratings(ArrayList<RatingModel> _user_ratings) {
        this._user_ratings = _user_ratings;
    }

    public ArrayList<VideoModel> get_user_video() {
        return _user_video;
    }

    public void set_user_video(ArrayList<VideoModel> _user_video) {
        this._user_video = _user_video;
    }


    public int get_user_subscribed() {
        return _user_subscribed;
    }

    public String get_user_birthday() {
        return _user_birthday;
    }

    public void set_user_birthday(String _user_birthday) {
        this._user_birthday = _user_birthday;
    }

    public int is_user_subscribed() {
        return _user_subscribed;
    }

    public void set_user_subscribed(int _user_subscribed) {
        this._user_subscribed = _user_subscribed;
    }

    public String get_user_verificationcode() {
        return _user_verificationcode;
    }

    public void set_user_verificationcode(String _user_verificationcode) {
        this._user_verificationcode = _user_verificationcode;
    }

    public String get_user_fblink() {
        return _user_fblink;
    }

    public void set_user_fblink(String _user_fblink) {
        this._user_fblink = _user_fblink;
    }

    public String get_user_twlink() {
        return _user_twlink;
    }

    public void set_user_twlink(String _user_twlink) {
        this._user_twlink = _user_twlink;
    }

    public String get_user_yolink() {
        return _user_yolink;
    }

    public void set_user_yolink(String _user_yolink) {
        this._user_yolink = _user_yolink;
    }

    public String get_user_inlink() {
        return _user_inlink;
    }

    public void set_user_inlink(String _user_inlink) {
        this._user_inlink = _user_inlink;
    }

    public String get_user_snlink() {
        return _user_snlink;
    }

    public void set_user_snlink(String _user_snlink) {
        this._user_snlink = _user_snlink;
    }

    public String get_user_aboutme() {
        return _user_aboutme;
    }

    public void set_user_aboutme(String _user_aboutme) {
        this._user_aboutme = _user_aboutme;
    }

    public String get_user_language() {
        return _user_language;
    }

    public void set_user_language(String _user_language) {
        this._user_language = _user_language;
    }

    public ArrayList<AvailableModel> get_user_available() {
        return _user_available;
    }

    public void set_user_available(ArrayList<AvailableModel> _user_available) {
        this._user_available = _user_available;
    }

    public String get_serve_location(){

        return _serve_location;
    }

    public void set_serve_location(String _serve_location){

        this._serve_location = _serve_location;
    }
}
