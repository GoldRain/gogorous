package com.sinmobile.gogorgeos.model;

import java.io.Serializable;

/**
 * Created by ToSuccess on 1/20/2017.
 */

public class VideoModel implements Serializable {

    int _video_id = 0;
    String _video_url = "";
    String _video_thumbimageurl = "";
    String _video_comment = "";

    public int get_video_id() {
        return _video_id;
    }

    public void set_video_id(int _video_id) {
        this._video_id = _video_id;
    }


    public String get_video_url() {
        return _video_url;
    }

    public void set_video_url(String _video_url) {
        this._video_url = _video_url;
    }

    public String get_video_thumbimageurl() {
        return _video_thumbimageurl;
    }

    public void set_video_thumbimageurl(String _video_thumbimageurl) {
        this._video_thumbimageurl = _video_thumbimageurl;
    }

    public String get_video_comment() {
        return _video_comment;
    }

    public void set_video_comment(String _video_comment) {
        this._video_comment = _video_comment;
    }
}
