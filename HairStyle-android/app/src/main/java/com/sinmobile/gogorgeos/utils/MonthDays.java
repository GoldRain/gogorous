package com.sinmobile.gogorgeos.utils;

import android.util.Log;

import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Commons;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static java.lang.Math.pow;

/**
 * Created by HugeRain on 2/19/2017.
 */



public class MonthDays {

    Calendar d = Calendar.getInstance();
    int year = d.get(Calendar.YEAR);
    int month = d.get(Calendar.MONTH);

    ArrayList<Long> _month_dates = new ArrayList<>();


    public int allDates(int year, int month){

        d = Calendar.getInstance();
        /*year = d.get(Calendar.YEAR);
        month = d.get(Calendar.MONTH);*/

        Log.d("===year=", String.valueOf(year));
        Log.d("====month==>", String.valueOf(month));

        int iYear = year; int iMonth = month; int iDay = 0;

        Calendar mycal = new GregorianCalendar(year, month, iDay);

        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);

        return daysInMonth;
    }

    public void initAvailableYear(int year){

        Commons.AVAILABLE_YEAR = new ArrayList<>();

        for (int month = 1; month < 13 ; month++){
            Calendar myCal = new GregorianCalendar(year, month, 0);
            int daysInMonth = myCal.getActualMaximum(Calendar.DAY_OF_MONTH);
            Commons.AVAILABLE_YEAR.add(getInitMonthValue(daysInMonth));

            Log.d("days in month ---", String.valueOf(daysInMonth));
            Log.d("init value ---", String.valueOf(getInitMonthValue(daysInMonth)));
        }
    }

    //All DAYS VALUE

    public long getInitMonthValue(Integer daysInMonth){

        daysInMonth += 1;
        return (long)pow((double)2,(double)daysInMonth) - 1;
    }

    public long removeDayFromMonthValue(Integer day, long prevValue){
        return prevValue -(prevValue & (long)pow((double)2,(double)day));
    }

    public long addDayToMonthValue(Integer day, long prevValue){
        return prevValue | (long)pow((double)2,(double)day);
    }



}
